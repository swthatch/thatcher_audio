﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class PlayerAudioTriggers : MonoBehaviour
{
    public AudioMixer mixer;
    public AudioMixerSnapshot[] snapshots;
    public float[] weights;
    public float transitionLength = 1f;

    public AudioSource scarysource;
    public AudioSource ghostsource;

    private float vollowRange = .5f;
    private float volHighRange = 1.0f;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("MusicTrigger"))
        {
            weights[0] = 0.0f;
            weights[1] = 1.0f;
            mixer.TransitionToSnapshots(snapshots, weights, transitionLength);
        }
        if (other.gameObject.CompareTag("ScaryTrigger"))
        {
            scarysource.volume = Random.Range(vollowRange, volHighRange);
            scarysource.pitch = Random.Range(vollowRange, volHighRange);
            scarysource.Play();
        }
        if (other.gameObject.CompareTag("GhostTrigger"))
        {
            ghostsource.volume = Random.Range(vollowRange, volHighRange);
            ghostsource.pitch = Random.Range(vollowRange, volHighRange);
            ghostsource.Play();

        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("MusicTrigger"))
        {
            weights[0] = 1.0f;
            weights[1] = 0.0f;
            mixer.TransitionToSnapshots(snapshots, weights, transitionLength);
        }
    }


}
